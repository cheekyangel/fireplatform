package kr.co.fireplatform.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainController {
	
	@RequestMapping(value="/")
	public String jrmsHompage() {
		return "redirect:http://jrms.co.kr";
	}
	
	@RequestMapping(value="fire")
	public String main() {
		return "main";
	}

}
