<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@600;700&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="/resources/css/nav.css">
	<script type="text/javascript" src="/resources/js/nav.js" defer></script>
	<title>Firefighting Platform</title>
</head>
<body>
	<nav class="navbar">
		<div class="navbar_logo">
			<!-- <img alt="fireplatform" src="/resources/img/logo.png"> -->
			<a href="/">F-Platform</a>
		</div>
		
		<ul class="navbar_menu">
			<li><a href="">안전정보</a></li>
			<li><a href="">소방점검</a></li>
			<li><a href="">완비증명</a></li>
			<li><a href="">시공</a></li>
			<li><a href="">설계</a></li>
			<li><a href="">제품</a></li>
			<li class="navbar_menu_user"><a href="">사용자정보</a></li>
		</ul>
		
		<div class="navbar_user">
			<img alt="user" src="/resources/img/img_user.png">
		</div>
		<a href="#" class="navbar_toggleBtn"><img alt="menu" src="/resources/img/img_hamburger.png"></a>
	</nav>
</body>
</html>